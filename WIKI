Al utilizar Packet Tracer y seguir la guia dada por el profesor se pudo aprender :

    1) Como Crear un Local Network para lo que puede ser por ejemplo una casa 
    2) El modificar un router, un servidor, conectar un pc a un wireless modem 
    3) encontrar la configuracion del IP, eliminar y renovar la misma a travez de comandos en el command Prompt
    4) Definir una IP estatica 
    5) Escoger un DHCP 
    6) Como configurar el nombre a una direccion IP 
    7) Como conectar todo a una direccion IP creada por nosotros mismos 
    8) Como manipular un servidor 
    9) como configurar el maximo de usuarios permitidos en la red 
    10) Como configurar una conexion por cable coaxial e Ethernet 
    11)Reemplazar un adaptador de Red para la Laptop 
    12) Colocar una tarjeta de un conector coaxial en el internet
    13 ) colocar una tarjeta de conexion rapiuda a internet 
    14) conectar una laptop al router 
    
Topologia : Para el proyecto de cisco lo que se uso fue un router inalambrico, un pc o computadora de escritorio conectada a traves de un
cable de ethernet, una laptop conectada a laa red a traves de wifi, un modem cableado concectado al router, 
el cual se conecto al internety de la misma manera se conecto el internet a un servidor de Cisco. 

El ejercicio consistio de crear una red simple desde zero en el espacio topologico de cisco packet tracer.

Para el ejercicio lo primero que toco hacer fue encontrar el router inalambrico con el fin de empezar la red
se configuro el dispositivo y su conexion al internet, y su direccion IP, siguiente a esto se coloco,
una Pc y una laptop, la pc se conecto de manera cableada al router y se le configuro la direccion IP para tener nuestra conexion a internet,
y para la laptop toco cambiar en el apartado fisico del computador,
el modulo de ethernet, y remplazarlo por el modulo WPC300N el cual nos daria la conexion inalambrica al router.
Empezamos a trabajar con la nube en la cual se le configuro las conexiones, agregandole un puerto de Ethernet 6 y un puerto Coazxial 7,
y finalmente se configuro el servidor de Cisco, se le configuro nuevamente la direccion IP, se prendio el servicio DHCP,
y finalmente se verifico la conectividad desde el computador, buscando la direccion IP la cual configuramos al principio del proyecto.

Laherraminetade simulacion Cisco Packet Tracer, es una herramienta la cual nos permite trabjar en un espacio topologico,
con el fin de poder crear conexiones de red ya sea para una casa, una oficina o incluso una ciudad, desde zeroso incluso trabajar coin proyectos 
preconstruidos. 


Video :  https://web.microsoftstream.com/video/cfa3767e-ce8e-43ae-97a1-89aedfda8ab9

Cisco. (n.d.). Packet Tracer–Create a Simple  NetworkUsing Packet Tracer . Cisco. https://unisabanaedu.sharepoint.com/sites/2272REDESYCOMUNICACINDEDATOS/_layouts/15/embed.aspx?uniqueId=88508479-86ae-4f9d-8444-d8251557431a&amp;access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvdW5pc2FiYW5hZWR1LnNoYXJlcG9pbnQuY29tQGFjYTUxNjMxLTAwZmUtNDkwZC05MWFiLTE2M2VmODcyNjBlZSIsImlzcyI6IjAwMDAwMDAzLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMCIsIm5iZiI6IjE2Mjk1MTc2NzYiLCJleHAiOiIxNjI5NTE3OTc2IiwiZW5kcG9pbnR1cmwiOiJRbDNtem15RFNGVEFYTzQwbW1WNUZ0ck1SNUZ4ZXNKcnovSXlmdGRPMm84PSIsImVuZHBvaW50dXJsTGVuZ3RoIjoiMTM5IiwiaXNsb29wYmFjayI6IlRydWUiLCJjaWQiOiJNalUyTVRsbFptVXROak5oTnkwME5ERmlMVGczWkdJdFlUa3hNbVV6T1RZd05EVXciLCJ2ZXIiOiJoYXNoZWRwcm9vZnRva2VuIiwic2l0ZWlkIjoiWVdZMU56Y3haVGt0TWpRelpDMDBObVJtTFRobFpXTXRaVGswT0dSbU9EbGpOR0psIiwiYXBwX2Rpc3BsYXluYW1lIjoiTWljcm9zb2Z0IFRlYW1zIiwiZ2l2ZW5fbmFtZSI6Ik1hdGVvIiwiZmFtaWx5X25hbWUiOiJNb250b3lhIFBhY2hvbiIsImFwcGlkIjoiMWZlYzhlNzgtYmNlNC00YWFmLWFiMWItNTQ1MWNjMzg3MjY0IiwidGlkIjoiYWNhNTE2MzEtMDBmZS00OTBkLTkxYWItMTYzZWY4NzI2MGVlIiwidXBuIjoibWF0ZW9tb3BhQHVuaXNhYmFuYS5lZHUuY28iLCJwdWlkIjoiMTAwMzIwMDA0RDYxNTIwRiIsImNhY2hla2V5IjoiMGguZnxtZW1iZXJzaGlwfDEwMDMyMDAwNGQ2MTUyMGZAbGl2ZS5jb20iLCJzY3AiOiJhbGxmaWxlcy53cml0ZSBhbGxzaXRlcy53cml0ZSIsInR0IjoiMiIsInVzZVBlcnNpc3RlbnRDb29raWUiOm51bGx9.MHE1RmNQdWFYYW5zN2kvdWxIVjFWZWpjUUdHZEhBVUF3eFN4SG1hQ3pFND0. 

